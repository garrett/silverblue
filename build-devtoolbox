#!/bin/bash
set -euo pipefail

# Use 'cockpit', unless an alternate name is specified
NAME="${1:-cockpit}"

# Location of the Cockpit tasks container
TOOLBOX_IMAGE="ghcr.io/cockpit-project/tasks"

# Work on host or even in a toolbox container
TOOLBOX_CMD=$(if [ -f "/run/.containerenv" ]; then
  echo "flatpak-spawn --host toolbox"
else
  echo "toolbox"
fi)

RPM=(
  # Friendly shell
  fish

  # Editors
  neovim codium

  # Browser
  chromium

  # Version control systems
  gh git gitg gitk lazygit meld

  # Development tools
  ansible meson ninja-build sassc ShellCheck

  # Testing and debugging tools
  standard-test-roles the_silver_searcher valgrind

  # Rust development
  cargo

  # Cockpit development deps
  libssh-devel pcp-devel

  # Ruby development
  ruby-devel rubygems

  # Graphics and multimedia tools
  ImageMagick libwebp-tools optipng pngquant waifu2x-converter-cpp

  # Networking tools
  iperf3 mosh redir youtube-dl

  # System utilities
  htop man-pages fzf

  # Language pack to fix UTF-8 issues
  glibc-langpack-en

  # Be able to easily escape the container using flatpak's spawn command
  flatpak-spawn

  # Anaconda needs Fedora logos
  fedora-logos
)

NPM=(
  npm jshint eslint
)

GEM=(
  irb rubocop solargraph
)

run_sudo() {
  local command="$1"
  #echo "-- Running: $TOOLBOX_CMD run --container \"$NAME\" sh -c \"sudo $command\""
  $TOOLBOX_CMD run --container "$NAME" sh -c "sudo $command"
}

create_container() {
  echo "## Creating container $NAME..."
  podman pull "$TOOLBOX_IMAGE"
  $TOOLBOX_CMD rm --force "$NAME" >/dev/null 2>&1 || true
  $TOOLBOX_CMD create --image "$TOOLBOX_IMAGE" "$NAME" >/dev/null 2>&1
}

fix_docs() {
  local DOC_FIX_RPM=(
    coreutils-common dnf\* gzip jq make openssh\* qemu\* rpm\* tmux \*virt\*
  )

  echo "## Fixing docs..."
  run_sudo "sed -i 's/nodocs//' /etc/dnf/dnf.conf"
  run_sudo "dnf --setopt=install_weak_deps=False -y reinstall ${DOC_FIX_RPM[*]}"
}

setup_vscodium_repo() {
  local repo_file="/etc/yum.repos.d/vscodium.repo"
  local gpg_key="https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg"
  local baseurl="https://download.vscodium.com/rpms/"

  echo "## Setting up VSCodium repository..."
  run_sudo "rpmkeys --import $gpg_key > /dev/null 2>&1"
  run_sudo "tee $repo_file <<EOF > /dev/null 2>&1
[gitlab.com_paulcarroty_vscodium_repo]
name=download.vscodium.com
baseurl=$baseurl
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=$gpg_key
metadata_expire=1h
EOF"
}

setup_lazygit_repo() {
  echo "## Setting up Lazygit repository..."
  run_sudo "dnf copr enable atim/lazygit -y > /dev/null 2>&1"
}

install_rpm() {
  echo "## Installing RPM packages..."
  run_sudo "dnf -y install ${RPM[*]}"
  run_sudo "dnf clean all"
}

install_npm() {
  echo "## Installing NPM packages..."
  run_sudo "npm install -g ${NPM[*]}"
}

install_gem() {
  echo "## Installing GEM packages..."
  run_sudo "gem install ${GEM[*]}"
  echo "## Updating RubyGems..."
  run_sudo "gem update --system"
}

echo "## Starting setup for container \"$NAME\"..."
create_container
fix_docs
setup_vscodium_repo
setup_lazygit_repo
install_rpm
install_npm
install_gem
echo "## Setup complete. Enter the container with \`toolbox enter \"$NAME\"\`"
