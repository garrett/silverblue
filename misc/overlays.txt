chromium
cockpit-system
cockpit-ws
docker
epiphany
file-roller
fish
gnome-boxes
gnome-tweaks
gnome-usage
gvfs-nfs
gvim
htop
hub
kexec-tools
libvirt-client
libvirt-dbus
npm
powertop
qemu
raw-thumbnailer
setroubleshoot-server
sushi
tuned-utils
v4l-utils
vim
virt-install
virt-manager
virt-viewer
